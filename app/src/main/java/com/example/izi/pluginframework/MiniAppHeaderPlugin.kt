package com.example.izi.pluginframework

import android.content.res.Resources
import android.view.View
import android.view.ViewGroup
import androidx.annotation.ColorRes
import androidx.annotation.IdRes

/**
 * Помогает настроить тулбар в миниаппах.
 *
 * Суть в том, что MotionLayout не работает со статус баром, а он у нас полупрозрачный.
 * Поэтому мы подставляем туда фейковую вью, размером со статус бар и цветом = основному цвету миниаппа
 *
 * Created by Igor Glushkov on 18/03/2020.
 */
class MiniAppHeaderPlugin(
    @IdRes private val appBarLayoutId: Int,
    @ColorRes private val statusBarColor: Int,
    @IdRes private val fragmentRootView: Int
) : BasePlugin {

    override fun onLifecycleEvent(event: LifecycleEvent) {
        when (event) {
            is LifecycleEvent.OnViewCreated -> {
                setupStatusBar(event.view)
            }
        }
    }

    private fun setupStatusBar(fragmentView: View) {
        fragmentView.setOnApplyWindowInsetsListener { _, windowInsets ->
            val appBarLayout = fragmentView.findViewById<View>(appBarLayoutId)
            val lp = appBarLayout.layoutParams as ViewGroup.MarginLayoutParams
            lp.topMargin = windowInsets.systemWindowInsetTop
            appBarLayout.layoutParams = lp
            val viewStatusBarHack = View(fragmentView.context)
            viewStatusBarHack.setBackgroundColor(fragmentView.resources.getColorCompat(statusBarColor))

            val screenWidth = Resources.getSystem().displayMetrics.widthPixels
            viewStatusBarHack.layoutParams = ViewGroup.LayoutParams(screenWidth, windowInsets.systemWindowInsetTop)
            fragmentView.findViewById<ViewGroup>(fragmentRootView).addView(viewStatusBarHack)
            windowInsets
        }
    }
}
