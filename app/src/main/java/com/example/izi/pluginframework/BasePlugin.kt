package com.example.izi.pluginframework

/** Механизм плагинов.
 * Плагины - классы, которые завязаны на ЖЦ активити / фрагмента.
 * Плагин получает событие об интересуемом его событии и делает некую логику, которую должен реализовать программист
 */
interface BasePlugin {
    fun onLifecycleEvent(event: LifecycleEvent)
}