package com.example.izi.domain.repository

import com.example.izi.data.model.YCompany
import com.example.izi.data.remote.YClientsApiService
import com.example.izi.presentation.dashboard.CompaniesRequestParams
import com.example.izi.presentation.dashboard.YClientsCompaniesDataSourceFactory
import io.reactivex.Single
import java.util.concurrent.Executor
import javax.inject.Inject

class YClientsRepository @Inject constructor(
        private val apiService: YClientsApiService,
        private val pagingListExecutor: Executor,
        private val resourcesStorage: ResourcesStorage
) {

    fun getCompanies(params: CompaniesRequestParams): Listing<YCompany> {
        val sourceFactory =
            YClientsCompaniesDataSourceFactory(
                params,
                apiService,
                pagingListExecutor
            )
        return Listing.create(sourceFactory, pagingListExecutor)
    }

    fun getCompanies(cityId: String): Single<ListResponse<YCompany>> {
        return apiService.getCompanies(cityId, DASHBOARD_LIMIT)
    }
}
