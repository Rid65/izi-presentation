package com.example.izi.di.module

import com.example.izi.data.remote.YClientsApiService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
open class ApiServiceModule {
    @Provides
    @Singleton
    open fun provideYClientsApiService(retrofit: Retrofit): YClientsApiService {
        return retrofit.create(YClientsApiService::class.java)
    }
}