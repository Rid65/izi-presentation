package com.example.izi.di.module

import androidx.lifecycle.ViewModel
import com.example.izi.di.util.ViewModelKey
import com.example.izi.presentation.dashboard.YClientsDashboardViewModel
import com.example.izi.presentation.dashboard.sort.SortCompaniesViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module(includes = [ViewModelFactoryModule::class])
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(YClientsDashboardViewModel::class)
    abstract fun yclientsDashboardViewModel(viewModel: YClientsDashboardViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SortCompaniesViewModel::class)
    abstract fun yclientsSortCompaniesViewModel(viewModel: SortCompaniesViewModel): ViewModel

}
