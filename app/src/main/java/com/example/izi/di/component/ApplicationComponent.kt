package com.example.izi.di.component

import com.example.izi.IziApplication
import com.example.izi.di.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ActivityModule::class,
        FragmentModule::class,
        ApiServiceModule::class,
        ApplicationModule::class,
        ViewModelModule::class
    ]
)
interface ApplicationComponent : AndroidInjector<IziApplication> {

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance applicationContext: IziApplication): ApplicationComponent
    }
}
