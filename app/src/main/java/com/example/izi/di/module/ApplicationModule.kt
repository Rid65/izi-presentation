package com.example.izi.di.module

import android.app.Application
import android.content.Context
import android.content.UriMatcher
import com.example.izi.IziApplication
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import dagger.Module
import dagger.Provides
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import javax.inject.Singleton

@Module
open class ApplicationModule {

    @Provides
    fun provideContext(app: IziApplication): Context {
        return app.applicationContext
    }

    @Provides
    fun provideApplication(app: IziApplication): Application {
        return app
    }

    @Provides
    fun provideDiffUtilItemCallbackFactory(): DiffUtilItemCallbackFactory {
        return DiffUtilItemCallbackFactory()
    }

    @Provides
    fun provideDiffUtilCallbackFactory(
        diffUtilItemCallbackFactory: DiffUtilItemCallbackFactory
    ): DiffUtilCallbackFactory {
        return DiffUtilCallbackFactory(diffUtilItemCallbackFactory)
    }

    @Provides
    fun providePagesListExecutor(): Executor = Executors.newFixedThreadPool(5)

    @Provides
    fun providesWorkingWayAdapter(): WorkHoursAdapter =
        WorkHoursAdapter()

    @Provides
    fun provideYDatetimeAdapter(): YDatetimeAdapter =
        YDatetimeAdapter()

    @Provides
    fun provideFusedLocationProvider(context: Context): FusedLocationProviderClient {
        return LocationServices.getFusedLocationProviderClient(context)
    }

    @Provides
    @Singleton
    fun provideUserLocationProvider(context: Context): UserLocationProvider {
        return UserLocationProvider(context)
    }

    @Provides
    fun pushNotificationHandlerChain(): PushNotificationHandlerChain {
        return PushNotificationHandlerChain()
    }

    @Provides
    fun deepLinkHandlerChain(resourcesStorage: ResourcesStorage): DeepLinkHandlerChain {
        return DeepLinkHandlerChain(resourcesStorage)
    }

    @Provides
    fun uriMatcher(): UriMatcher = UriMatcher(UriMatcher.NO_MATCH)

}
