package com.example.izi.di.module

import com.example.izi.presentation.dashboard.YClientsDashboardFragment
import com.example.izi.presentation.dashboard.sort.SortCompaniesFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module(includes = [ViewModelModule::class])
abstract class FragmentModule {

    @ContributesAndroidInjector
    abstract fun yclientsDashboardFragment(): YClientsDashboardFragment

    @ContributesAndroidInjector
    abstract fun yclientsSortCompaniesFragment(): SortCompaniesFragment

}
