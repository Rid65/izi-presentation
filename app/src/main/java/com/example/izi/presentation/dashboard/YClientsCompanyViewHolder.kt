package com.example.izi.presentation.dashboard

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.updateLayoutParams
import androidx.recyclerview.widget.RecyclerView
import com.example.izi.R
import com.example.izi.data.model.YCompany

class YClientsCompanyViewHolder(
        parent: ViewGroup,
        private val clickListener: (YCompany?) -> Unit,
        mode: CompanyViewHolderMode
) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.item_yclients_company, parent, false).apply {
        updateLayoutParams { width = mode.width }
    }
) {
    fun bind(yCompany: YCompany?) = with(itemView) {
        textViewYClientsTitle.text = yCompany?.title
        textViewYClientsCategory.text = yCompany?.shortDescription
        imageViewYClientsCompany.loadWithMaskTransformationScaled(yCompany?.image, R.drawable.bg_yclients_company_mask)
        setOnClickListener { clickListener(yCompany) }
    }
}

enum class CompanyViewHolderMode(val width: Int) {

    MAIN_DASHBOARD(ViewGroup.LayoutParams.WRAP_CONTENT),
    YCLIENTS_DASHBOARD(ViewGroup.LayoutParams.MATCH_PARENT);
}