package com.example.izi.presentation.dashboard

import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import com.example.izi.data.model.YCompany
import javax.inject.Inject

class YClientsCompaniesAdapter @Inject constructor(
    diffUtilItemCallbackFactory: DiffUtilItemCallbackFactory
) : PagedListAdapter<YCompany, YClientsCompanyViewHolder>(diffUtilItemCallbackFactory.create()) {

    var clickListener: (YCompany?) -> Unit = {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        YClientsCompanyViewHolder(
            parent,
            clickListener,
            CompanyViewHolderMode.YCLIENTS_DASHBOARD
        )

    override fun onBindViewHolder(holder: YClientsCompanyViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}
