package com.example.izi.presentation.views

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Point
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.ViewPropertyAnimator
import android.widget.FrameLayout
import io.reactivex.Completable
import io.reactivex.schedulers.Schedulers
import kotlin.math.roundToInt

/**
 * Вью, которая реагирует на клик только в непрозрачных областях
 * */
abstract class NonTransparencyClickableView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    /** нужен для определения прозрачности пикселов во время касания кнопки */
    protected var bitmap: Bitmap? = null

    abstract fun actionDownAnimation(): ViewPropertyAnimator?
    abstract fun actionUpAnimation(): ViewPropertyAnimator?

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        initBitmap()
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when (event?.action) {
            MotionEvent.ACTION_DOWN -> {
                // касание по прозрачной области не обрабатываем
                val point = Point(event.x.roundToInt(), event.y.roundToInt())
                if (bitmap == null || bitmap?.isTransparentPixel(point.x, point.y) == false) {
                    // Если пиксель не прозрачный
                    actionDownAnimation()?.start()
                } else {
                    return false
                }
            }
            MotionEvent.ACTION_MOVE -> {
                actionDownAnimation()?.start()
            }
            else -> {
                actionUpAnimation()?.start()
            }
        }
        return super.onTouchEvent(event)
    }

    /** Создаем bitmap из текущей вью, чтобы потом выполнить проверку касания на непрозрачный пиксел */
    private fun initBitmap() {
        if (bitmap == null) {
            Completable.fromRunnable {
                bitmap = Bitmap.createBitmap(measuredWidth, measuredHeight, Bitmap.Config.ARGB_8888).apply {
                    val canvas = Canvas(this)
                    this@NonTransparencyClickableView.draw(canvas)
                }
            }
                    .subscribeOn(Schedulers.io())
                    .subscribe()
        }
    }

    private fun Bitmap.isTransparentPixel(x: Int, y: Int): Boolean {
        val alpha = ((this.getPixel(x, y).toLong() and 0xff000000) shr 24).toInt()
        return alpha == 0
    }
}