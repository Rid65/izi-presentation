package com.example.izi.presentation.dashboard

import com.example.izi.data.model.YCompany
import com.example.izi.data.remote.YClientsApiService
import io.reactivex.Single
import java.util.concurrent.Executor

class YClientsCompaniesDataSourceFactory(
        private val params: CompaniesRequestParams,
        apiService: YClientsApiService,
        retryExecutor: Executor
) : BasePositionalDataSourceFactory<YCompany, YClientsCompaniesPositionalDataSource, YClientsApiService>(
    apiService, retryExecutor
) {
    override fun createDataSource(
        apiService: YClientsApiService,
        retryExecutor: Executor
    ): YClientsCompaniesPositionalDataSource {
        return YClientsCompaniesPositionalDataSource(
            params,
            apiService,
            retryExecutor
        )
    }
}

class YClientsCompaniesPositionalDataSource(
    private val params: CompaniesRequestParams,
    private val apiService: YClientsApiService,
    retryExecutor: Executor
) : BasePositionalDataSource<YCompany>(retryExecutor) {
    override fun loadPage(offset: Int, limit: Int): Single<ListResponse<YCompany>> {
        return apiService.getCompanies(params.cityId, limit, offset, params.query, params.lat, params.lng, params.sort)
    }
}
