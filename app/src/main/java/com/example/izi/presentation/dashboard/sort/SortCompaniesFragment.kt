package com.example.izi.presentation.dashboard.sort

import android.app.Dialog
import android.os.Bundle
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.view.isVisible
import com.example.izi.R
import com.example.izi.data.model.YCompanySort
import com.example.izi.presentation.base.BaseBottomSheetDialogFragment
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.fragment_sort_companies.textViewSortCompaniesAlphabetAsc
import kotlinx.android.synthetic.main.fragment_sort_companies.textViewSortCompaniesAlphabetDesc
import kotlinx.android.synthetic.main.fragment_sort_companies.textViewSortCompaniesDistance

class SortCompaniesFragment : BaseBottomSheetDialogFragment() {

    private val viewModel: SortCompaniesViewModel by iziActivityViewModels()

    override val layoutResId: Int
        get() = R.layout.fragment_sort_companies

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return BottomSheetDialog(requireContext(), R.style.TransparentBottomSheetDialogTheme)
    }

    override fun callInitOperations() {
        viewModel.checkLocation()
    }

    override fun onSetupLayout(savedInstanceState: Bundle?) {
        textViewSortCompaniesDistance.setOnClickListener {
            viewModel.setSort(YCompanySort.DISTANCE)
            dismiss()
        }
        textViewSortCompaniesAlphabetAsc.setOnClickListener {
            viewModel.setSort(YCompanySort.TITLE_ASC)
            dismiss()
        }
        textViewSortCompaniesAlphabetDesc.setOnClickListener {
            viewModel.setSort(YCompanySort.TITLE_DESC)
            dismiss()
        }
    }

    override fun onBindViewModel() = with(viewModel) {
        userLocationLiveData.observe {
            textViewSortCompaniesDistance.isVisible = it != null
        }
    }
}
