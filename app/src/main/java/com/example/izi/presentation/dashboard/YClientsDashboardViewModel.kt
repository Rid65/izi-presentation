package com.example.izi.presentation.dashboard

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import androidx.lifecycle.switchMap
import com.example.izi.data.LoadableResult
import com.example.izi.data.model.YCompanySort
import com.example.izi.presentation.base.BaseViewModel
import javax.inject.Inject

class YClientsDashboardViewModel @Inject constructor(
    private val cityRepository: CityRepository,
    private val yclientsRepository: YClientsRepository,
    private val userLocationProvider: UserLocationProvider
) : BaseViewModel() {

    val miniAppId = MiniAppId.YCLIENTS

    // текущий город, используется только для отображения. Смена города слушается по другому
    private val _cityTitleLiveData = MutableLiveData(cityRepository.getMiniAppCity(miniAppId))
    val cityTitleLiveData: LiveData<City> = _cityTitleLiveData

    // используется для определения того, поддерживается ли выбранный город миниаппом
    private val _cityAvailableLiveData = MutableLiveData<LoadableResult<Boolean>>()
    val cityAvailableLiveData: LiveData<LoadableResult<Boolean>> = _cityAvailableLiveData

    // LiveData для основного списка компаний (хранится отдельно от поискового списка)
    private val companiesRequestParams = MutableLiveData<CompaniesRequestParams>()
    private val companiesPagingLiveData = companiesRequestParams.map {
        yclientsRepository.getCompanies(it)
    }
    val companiesStateLiveData = companiesPagingLiveData.switchMap { it.initialState }
    val companiesListLiveData = companiesPagingLiveData.switchMap { it.pagedList }

    // LiveData для списка компаний, полученных поисковым запросом
    private val searchCompaniesRequestParams = MutableLiveData<CompaniesRequestParams>()
    private val searchCompaniesPagingLiveData = searchCompaniesRequestParams.map {
        yclientsRepository.getCompanies(it)
    }
    val searchCompaniesListLiveData = searchCompaniesPagingLiveData.switchMap { it.pagedList }
    val searchCompaniesStateLiveData = searchCompaniesPagingLiveData.switchMap { it.initialState }

    fun changeCity(city: City) {
        cityRepository.setMiniAppCity(miniAppId, city)
        _cityTitleLiveData.value = city
        getCompaniesIfCityAvailable()
    }

    fun getCompaniesIfCityAvailable() {
        val cityId = cityRepository.getMiniAppCity(miniAppId).id
        cityRepository.checkIfCityAvailable(miniAppId, cityId)
            .doOnSuccess { isCityAvailable ->
                if (isCityAvailable) getCompanies(
                    CompaniesRequestParams(
                        cityId
                    )
                )
            }.execute(_cityAvailableLiveData)
    }

    fun searchCompanies(query: String) {
        val cityId = cityRepository.getMiniAppCity(miniAppId).id
        searchCompaniesRequestParams.postValue(
            CompaniesRequestParams(
                cityId = cityId,
                query = query
            )
        )
    }

    fun sortWasChanged(sort: YCompanySort) {
        val cityId = cityRepository.getMiniAppCity(miniAppId).id
        val params = companiesRequestParams.value ?: CompaniesRequestParams(
            cityId
        )
        when (sort) {
            YCompanySort.DISTANCE -> {
                val location = userLocationProvider.lastKnownLocation
                getCompanies(params.copy(sort = sort, lat = location?.latitude, lng = location?.longitude))
            }
            YCompanySort.TITLE_ASC -> {
                getCompanies(params.copy(sort = sort))
            }
            YCompanySort.TITLE_DESC -> {
                getCompanies(params.copy(sort = sort))
            }
        }
    }

    fun hideSearchedList() {
        (companiesListLiveData as MutableLiveData).postValue(companiesListLiveData.value)
    }

    private fun getCompanies(params: CompaniesRequestParams) {
        companiesRequestParams.postValue(params)
    }
}
