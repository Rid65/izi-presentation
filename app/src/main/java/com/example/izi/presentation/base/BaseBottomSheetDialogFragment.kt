package com.example.izi.presentation.base

import android.app.Activity
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.MainThread
import androidx.fragment.app.Fragment
import androidx.fragment.app.createViewModelLazy
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.android.support.AndroidSupportInjection
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import javax.inject.Inject
import timber.log.Timber

abstract class BaseBottomSheetDialogFragment : BottomSheetDialogFragment() {

    abstract val layoutResId: Int
    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewBindingsDisposable: CompositeDisposable

    @SuppressWarnings("deprecation")
    override fun onAttach(activity: Activity) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            AndroidSupportInjection.inject(this)
        }
        super.onAttach(activity)
    }

    override fun onAttach(context: Context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            AndroidSupportInjection.inject(this)
        }
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        callInitOperations()
        Timber.d("onCreate $this")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return if (view != null) view else inflater.inflate(layoutResId, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewBindingsDisposable = CompositeDisposable()
        Timber.d("onActivityCreated $this")
        onSetupLayout(savedInstanceState)
        onBindViewModel()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Timber.d("onDestroyView $this")
        viewBindingsDisposable.clear()
    }

    override fun onDestroy() {
        Timber.d("onDestroy $this")
        super.onDestroy()
    }

    /**
     * Вызывать методы вью модели, которые получают данные из репозиториев
     */
    abstract fun callInitOperations()

    /**
     * В этом методе производить все настройки вью фрагмента
     * установка OnClickListener, RecyclerView.Adapter/LayoutManager, etc
     */
    abstract fun onSetupLayout(savedInstanceState: Bundle?)

    /**
     * В этом методе производить подписки на изменения значений в лайвдатах у ViewModel
     */
    abstract fun onBindViewModel()

    protected infix fun <T> LiveData<T>.observe(block: (T) -> Unit) {
        observe(this@BaseBottomSheetDialogFragment, Observer<T> { t -> block.invoke(t) })
    }

    protected fun <T> observeSavedStateHandleLiveData(code: String, block: (T) -> Unit) {
        findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData<T>(code)?.observe { t ->
            block.invoke(t)
            findNavController().currentBackStackEntry?.savedStateHandle?.remove<T>(code)
        }
    }

    protected fun Disposable.autoDispose() {
        viewBindingsDisposable.add(this)
    }

    @MainThread
    inline fun <reified VM : ViewModel> Fragment.iziViewModels() =
        createViewModelLazy(VM::class, { this.viewModelStore }, { viewModelFactory })

    @MainThread
    inline fun <reified VM : ViewModel> Fragment.iziActivityViewModels() =
        createViewModelLazy(VM::class, { requireActivity().viewModelStore }, {
            if (requireActivity() is BaseActivity) {
                (requireActivity() as BaseActivity).viewModelFactory
            } else {
                requireActivity().defaultViewModelProviderFactory
            }
        })
}
