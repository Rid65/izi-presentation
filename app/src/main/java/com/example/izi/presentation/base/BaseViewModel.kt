package com.example.izi.presentation.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.izi.data.LoadableResult
import com.example.izi.data.convert
import com.example.izi.extensions.async
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseViewModel : ViewModel() {

    protected val compositeDisposable = CompositeDisposable()

    protected fun <T> Single<T>.execute(resultLiveData: MutableLiveData<LoadableResult<T>>): Disposable {
        return async()
            .convert {
                resultLiveData.postValue(it)
            }
            .autoDispose()
    }

    protected fun <T> Single<T>.execute(block: (LoadableResult<T>) -> Unit): Disposable {
        return async()
            .convert {
                block(it)
            }
            .autoDispose()
    }

    protected fun <T> Flowable<T>.execute(resultLiveData: MutableLiveData<LoadableResult<T>>): Disposable {
        return async()
            .convert {
                resultLiveData.postValue(it)
            }
            .autoDispose()
    }

    protected fun <T> Flowable<T>.execute(block: (LoadableResult<T>) -> Unit): Disposable {
        return async()
            .convert {
                block.invoke(it)
            }
            .autoDispose()
    }

    protected fun <T> Observable<T>.execute(resultLiveData: MutableLiveData<LoadableResult<T>>): Disposable {
        return async()
            .convert {
                resultLiveData.postValue(it)
            }
            .autoDispose()
    }

    protected fun <T> Observable<T>.execute(block: (LoadableResult<T>) -> Unit): Disposable {
        return async()
            .convert {
                block.invoke(it)
            }
            .autoDispose()
    }

    protected fun <T> Maybe<T>.execute(defaultValue: T, resultLiveData: MutableLiveData<LoadableResult<T>>): Disposable {
        return async()
            .convert(defaultValue) {
                resultLiveData.postValue(it)
            }
            .autoDispose()
    }

    protected fun <T> Maybe<T>.execute(defaultValue: T, block: (LoadableResult<T>) -> Unit): Disposable {
        return async()
            .convert(defaultValue) {
                block.invoke(it)
            }
            .autoDispose()
    }

    override fun onCleared() {
        compositeDisposable.dispose()
        super.onCleared()
    }

    protected fun Disposable.autoDispose(): Disposable {
        compositeDisposable.add(this)
        return this
    }
}
