package com.example.izi.presentation.dashboard.sort

import android.location.Location
import androidx.lifecycle.LiveData
import com.example.izi.data.model.YCompanySort
import com.example.izi.presentation.base.BaseViewModel
import com.example.izi.presentation.base.SingleLiveEvent
import javax.inject.Inject

class SortCompaniesViewModel @Inject constructor(
    private val userLocationProvider: UserLocationProvider
) : BaseViewModel() {

    private val _sortCompaniesLiveEvent = SingleLiveEvent<YCompanySort>()
    val sortCompaniesLiveData: LiveData<YCompanySort> = _sortCompaniesLiveEvent

    private val _userLocationLiveEvent = SingleLiveEvent<Location?>()
    val userLocationLiveData: LiveData<Location?> = _userLocationLiveEvent

    fun setSort(sort: YCompanySort) {
        _sortCompaniesLiveEvent.value = sort
    }

    fun checkLocation() {
        _userLocationLiveEvent.value = userLocationProvider.lastKnownLocation
    }
}
