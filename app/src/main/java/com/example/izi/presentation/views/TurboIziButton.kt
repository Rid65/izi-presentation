package com.example.izi.presentation.views

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.view.ViewPropertyAnimator
import android.view.animation.AnimationUtils
import androidx.core.content.ContextCompat
import com.example.izi.R
import com.example.izi.extensions.getColorCompat
import java.util.*

class TurboIziButton : NonTransparencyClickableView {

    companion object {
        private const val DEFAULT_TEXT_COLOR_ID = R.color.black
        private const val DEFAULT_TEXT_GRAVITY = Gravity.TOP or Gravity.START
        private const val DEFAULT_TEXT_ALL_CAPS = true
    }

    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet?) {
        LayoutInflater.from(context).inflate(R.layout.view_button_turbo_izi, this, true)

        var textColor = resources.getColorCompat(DEFAULT_TEXT_COLOR_ID)
        var textGravity = DEFAULT_TEXT_GRAVITY
        var text: String? = ""
        var textAllCaps = DEFAULT_TEXT_ALL_CAPS

        attrs?.let {
            context.obtainStyledAttributes(it, R.styleable.TurboIziButton).apply {
                textColor = getColor(R.styleable.TurboIziButton_textColor, textColor)
                textGravity = getInt(R.styleable.TurboIziButton_textGravity, textGravity)
                text = getString(R.styleable.TurboIziButton_text)
                textAllCaps = getBoolean(R.styleable.TurboIziButton_textAllCaps, textAllCaps)
                recycle()
            }
        }

        textViewTurboName.setTextColor(textColor)
        textViewTurboName.text = text
        textViewTurboName.isAllCaps = textAllCaps
        (textViewTurboName.layoutParams as LayoutParams).gravity = textGravity
    }

    override fun actionDownAnimation(): ViewPropertyAnimator? {
        return animate().alpha(0.6f).setDuration(20)
    }

    override fun actionUpAnimation(): ViewPropertyAnimator? {
        return animate().alpha(1f).setDuration(20)
    }

    fun beginAnimation() {
        AnimationUtils.loadAnimation(context, R.anim.anim_buttons).apply {
            startAnimation(this)
        }
        animateText()
    }

    private fun animateText() {
        textViewTurboName.startAnimation(
                when ((textViewTurboName.layoutParams as LayoutParams).gravity) {
                    Gravity.TOP or Gravity.START -> {
                        AnimationUtils.loadAnimation(context, R.anim.anim_transition_to_top_start)
                    }
                    Gravity.TOP or Gravity.END -> {
                        AnimationUtils.loadAnimation(context, R.anim.anim_transition_to_top_end)
                    }
                    Gravity.BOTTOM or Gravity.START -> {
                        AnimationUtils.loadAnimation(context, R.anim.anim_transition_to_bottom_start)
                    }
                    else -> {
                        AnimationUtils.loadAnimation(context, R.anim.anim_transition_to_bottom_end)
                    }
                }
        )
    }

    private fun customizeByName(name: MiniAppId) {
        when (name) {
            MiniAppId.YCLIENTS -> {
                backgroundTintList = ContextCompat.getColorStateList(context, R.color.bg_beauty_button)
                textViewTurboName.setTextColor(ContextCompat.getColor(context, android.R.color.black))
                textViewTurboName.text = context.getString(R.string.turboizi_yclients)
            }
            // and other branch...
        }
    }

    fun customizeButton(config: MiniAppConfig) {
        tag = config.alias
        if (!config.backgroundColor.isNullOrBlank() &&
                !config.textColor.isNullOrBlank() &&
                !config.name.isNullOrBlank()) {
            backgroundTintList = ColorStateList.valueOf(Color.parseColor(config.backgroundColor))
            textViewTurboName.setTextColor(Color.parseColor(config.textColor))
            textViewTurboName.text = config.name.toLowerCase(Locale.getDefault())
        } else {
            customizeByName(config.alias)
        }
    }
}