package com.example.izi.presentation.views

import android.content.Context
import android.text.InputType
import android.util.AttributeSet
import androidx.annotation.Keep
import androidx.annotation.StringRes
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import com.example.izi.extensions.async
import com.example.izi.extensions.clearButton
import com.example.izi.extensions.searchEditText
import com.jakewharton.rxbinding3.widget.textChanges
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit

// Кастомизируемая нативная вьюха поиска
@Keep
class CustomSearchView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : SearchView(context, attrs, defStyleAttr) {

    var subscribeCallback: ((searchView: SearchView, query: String) -> Unit)? = null
    var disposable: Disposable? = null
    /** Признак того, что первый пустой поисковый запрос уже прошел.
     * Нужен, чтобы не пропускать первый пустой запрос после открытия поиска */
    var firstEmptyQueryWasPassed: Boolean = false

    init {
        inputType = InputType.TYPE_TEXT_FLAG_CAP_SENTENCES

        searchEditText.apply {
            disposable = textChanges()
                .skipInitialValue()
                .filter {
                    // Не пропускаем первый пустой запрос
                    if (!firstEmptyQueryWasPassed) {
                        firstEmptyQueryWasPassed = true
                        return@filter false
                    }
                    true
                }
                .map {
                    // Строка ниже делает видимой кнопку Очистить поиска
                    if (it.isNotEmpty()) this@CustomSearchView.requestLayout()
                    it
                }
                .debounce(500, TimeUnit.MILLISECONDS)
                .async()
                .subscribe(
                    { query -> subscribeCallback?.invoke(this@CustomSearchView, query.toString()) },
                    { it.printStackTrace() }
                )
        }
    }

    fun setHint(@StringRes hintId: Int) {
        queryHint = context.getString(hintId)
    }

    // Применяет белый цвет ко всем элементам
    fun setMode(mode: DashboardSearchViewMode) {
        clearButton.setImageResource(mode.clearButton)
        searchEditText.setHintTextColor(ContextCompat.getColor(context, mode.hintColor))
        searchEditText.setTextColor(ContextCompat.getColor(context, mode.textColor))
    }
}

// Кастомизация элементов строки поиска
enum class DashboardSearchViewMode(val clearButton: Int, val hintColor: Int, val textColor: Int) {

    WHITE(R.drawable.ic_close, R.color.white, R.color.white),
    GRAY(R.drawable.ic_close, R.color.brown_grey_two, R.color.white),
    BLACK(R.drawable.ic_close_black, R.color.black, R.color.black);
}