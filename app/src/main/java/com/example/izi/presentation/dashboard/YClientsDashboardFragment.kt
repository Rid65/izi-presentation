package com.example.izi.presentation.dashboard

import android.os.Bundle
import android.view.MenuItem
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.core.content.ContextCompat
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.izi.R
import com.example.izi.data.LoadableResult
import com.example.izi.data.Success
import com.example.izi.pluginframework.MiniAppHeaderPlugin
import com.example.izi.presentation.base.BaseFragment
import com.example.izi.presentation.dashboard.sort.SortCompaniesViewModel
import com.example.izi.presentation.views.CustomSearchView
import com.example.izi.presentation.views.DashboardSearchViewMode
import javax.inject.Inject

class YClientsDashboardFragment : BaseFragment() {

    private val viewModel: YClientsDashboardViewModel by iziViewModels()
    private val sortCompaniesViewModel: SortCompaniesViewModel by iziActivityViewModels()
    private val selectCitySharedViewModel: SelectCitySharedViewModel by iziActivityViewModels()
    private val args: YClientsDashboardFragmentArgs by navArgs()
    private val miniAppId = MiniAppId.YCLIENTS
    @Inject lateinit var companiesAdapter: YClientsCompaniesAdapter
    private lateinit var backPressedCallback: OnBackPressedCallback

    private lateinit var textViewCityTitle: TextView
    private lateinit var customSearchView: CustomSearchView
    private lateinit var cityMenuItem: MenuItem
    private lateinit var searchMenuItem: MenuItem

    override val layoutResId: Int
        get() = R.layout.fragment_yclients_dashboard

    override fun initPlugins() {
        super.initPlugins()
        addPlugin(
            MiniAppHeaderPlugin(
                R.id.appBarLayoutYClientsDashboard,
                R.color.aqua_marine,
                R.id.viewYClientsDashboardContainer
            )
        )
    }

    override fun callInitOperations() {
        args.destination?.let { routeToDestination(it) }
        viewModel.getCompaniesIfCityAvailable()
    }

    override fun onSetupLayout(savedInstanceState: Bundle?) {
        setupToolbar()
        setupHeader()
        setupSearch()
        setupRecyclerView()

        viewFlipperYClientsDashboard.setStateFromResult(LoadableResult.success(null))
        viewFlipperYClientsDashboard.setRetryMethod {
            viewModel.getCompaniesIfCityAvailable()
        }
        // чтобы при ошибке кнопка снизу не вылазила за пределы экрана
        viewFlipperYClientsDashboard.errorView.updatePadding(bottom = resources.getDimensionPixelSize(R.dimen.header_height) * 2)

        swipeRefreshYClientsDashboard.setup(recyclerViewYClientsDashboard, companiesAdapter) {
            viewModel.getCompaniesIfCityAvailable()
        }

        // Переопределяем обратный вызов системной кнопки Back
        backPressedCallback = setOnBackPressedListener { hideSearchView() }.also { it.isEnabled = false }
    }

    override fun onBindViewModel() {
        with(viewModel) {
            cityAvailableLiveData.observe { result ->
                if (result is Success) {
                    val isCityAvailable = result.value
                    if (!isCityAvailable) {
                        setEmptyViewCityNotAvailableState()
                        viewFlipperYClientsDashboard.setCustomState()
                    }
                } else {
                    viewFlipperYClientsDashboard.setStateFromResult(result)
                }
            }

            cityTitleLiveData.observe {
                textViewCityTitle.text = it.title
            }

            selectCitySharedViewModel.selectCityLiveEvent.observe { city ->
                viewModel.changeCity(city)
            }

            companiesListLiveData.observe {
                setEmptyViewCompaniesState()
                companiesAdapter.submitList(it) { recyclerViewYClientsDashboard.checkIfEmpty() }
            }

            companiesStateLiveData.observe {
                viewFlipperYClientsDashboard.setStateFromResult(it)
            }

            searchCompaniesListLiveData.observe {
                setEmptyViewSearchState()
                companiesAdapter.submitList(it) { recyclerViewYClientsDashboard.checkIfEmpty() }
            }

            searchCompaniesStateLiveData.observe {
                viewFlipperYClientsDashboard.setStateFromResult(it)
            }
        }

        with(sortCompaniesViewModel) {
            sortCompaniesLiveData.observe {
                viewModel.sortWasChanged(it)
            }
        }
    }

    private fun setupToolbar() {
        textViewCityTitle = (toolbarYClientsDashboard.menu.findItem(R.id.action_city).actionView as ToolbarCityBlackTextView)
            .findViewById(R.id.textViewToolbarCity)
        toolbarYClientsDashboard.setupClose(isBlack = true) {
            findNavController().popBackStack()
        }
        toolbarYClientsDashboard.menu?.let {
            searchMenuItem = it.findItem(R.id.action_search)
            cityMenuItem = it.findItem(R.id.action_city)
        }
        textViewCityTitle.setTextColor(ContextCompat.getColor(requireContext(), R.color.charcoal_two))
        textViewCityTitle.setCompoundDrawablesRelativeWithIntrinsicBounds(
            0,
            0,
            R.drawable.ic_edit_black,
            0
        )

        textViewCityTitle.setOnClickListener {
            findNavController().navigate(
                YClientsDashboardFragmentDirections.actionGlobalSelectCityFragment(
                    SelectCityFragment.ScreenBehavior.CURRENT_MINI_APP_CITY,
                    miniAppId
                )
            )
        }
    }

    private fun setupSearch() {
        // Настраиваем SearchView
        customSearchView = (searchMenuItem.actionView as CustomSearchView).apply {
            subscribeCallback = { _, query ->
                // Отправляем запрос на поиск только если он в состоянии Expanded
                if (!isIconified) viewModel.searchCompanies(query)
            }
            disposable?.autoDispose()
            setHint(R.string.yclients_dashboard_searchview_hint)
            setMode(DashboardSearchViewMode.BLACK)
        }

        searchMenuItem.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
                cityMenuItem.isVisible = false
                swipeRefreshYClientsDashboard.isEnabled = false
                backPressedCallback.isEnabled = true
                customSearchView.firstEmptyQueryWasPassed = false
                return true
            }

            override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
                cityMenuItem.isVisible = true
                appBarLayoutYClientsDashboard.setExpanded(true, true)
                viewModel.hideSearchedList()
                swipeRefreshYClientsDashboard.isEnabled = true
                backPressedCallback.isEnabled = false
                customSearchView.firstEmptyQueryWasPassed = false
                return true
            }
        })
    }

    private fun setupHeader() {
        // отключаем коллапсинг шапки по свайпу
        appBarLayoutYClientsDashboard.setScrollingEnabled(false)
        imageButtonYClientsDashboardSearch.setOnClickListener {
            showSearchView()
        }
        imageButtonYClientsDashboardSort.setOnClickListener {
            findNavController().navigate(
                YClientsDashboardFragmentDirections.actionYclientsDashboardFragmentToSortCompaniesFragment()
            )
        }
    }

    private fun setupRecyclerView() = with(recyclerViewYClientsDashboard) {
        itemAnimator = null
        // Чтобы шапка не скролилась вместе со списком
        isNestedScrollingEnabled = false
        adapter = companiesAdapter
        emptyView = emptyViewYClientsDashboardCompanies
        companiesAdapter.clickListener = {
            findNavController().navigate(
                YClientsDashboardFragmentDirections.actionYclientsDashboardFragmentToYClientsCompanyFragment("1")
            )
        }
    }

    private fun routeToDestination(destination: YClientsAppRoutingDestination) {
        val navOptions = NavOptions.Builder()
            .setPopUpTo(
                destination.popUpDestinationId ?: R.id.yclients_app,
                destination.popUpInclusive ?: true
            ).build()

        when (destination) {
            is YClientsOrderDestination -> findNavController().navigate(
                YClientsDashboardFragmentDirections.actionYclientsDashboardFragmentToYClientsOrderFragment(
                    destination.orderId
                ),
                navOptions
            )
            is YClientsCompanyDestination -> findNavController().navigate(
                YClientsDashboardFragmentDirections.actionYclientsDashboardFragmentToYClientsCompanyFragment(
                    companyId = destination.companyId
                ),
                navOptions
            )
        }
    }

    private fun showSearchView() {
        appBarLayoutYClientsDashboard.setExpanded(false, true)
        searchMenuItem.expandActionView()
    }

    private fun hideSearchView() {
        appBarLayoutYClientsDashboard.setExpanded(true, true)
        searchMenuItem.collapseActionView()
    }

    private fun setEmptyViewCityNotAvailableState() = with(emptyViewYClientsDashboardCityNotAvailable) {
        // if not available in current city.
        setEmptyMessage(R.string.not_work_in_city)
        setButtonTextAndClickListener(R.string.change_city_request) {
            findNavController().navigate(
                YClientsDashboardFragmentDirections.actionGlobalSelectCityFragment(
                    SelectCityFragment.ScreenBehavior.CURRENT_MINI_APP_CITY,
                    miniAppId
                )
            )
        }
    }

    private fun setEmptyViewSearchState() = with(emptyViewYClientsDashboardCompanies) {
        setEmptyMessage(R.string.yclients_dashboard_search_empty)
    }

    private fun setEmptyViewCompaniesState() = with(emptyViewYClientsDashboardCompanies) {
        setEmptyMessage(R.string.empty_default_message)
    }
}
