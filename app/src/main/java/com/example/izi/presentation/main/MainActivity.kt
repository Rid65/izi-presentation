package com.example.izi.presentation.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.example.izi.R
import com.example.izi.presentation.base.BaseActivity

class MainActivity : BaseActivity() {

    companion object {
        fun createStartIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}