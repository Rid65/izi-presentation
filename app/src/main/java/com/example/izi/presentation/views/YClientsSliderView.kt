package com.example.izi.presentation.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.izi.R
import kotlinx.android.synthetic.main.view_yclients_slider.view.constraintLayoutSliderHeader
import kotlinx.android.synthetic.main.view_yclients_slider.view.frameLayoutYClientsSliderEmptyViewContainer
import kotlinx.android.synthetic.main.view_yclients_slider.view.recyclerViewSlider
import kotlinx.android.synthetic.main.view_yclients_slider.view.textViewSliderTitle
import kotlinx.android.synthetic.main.view_yclients_slider.view.viewFlipperSlider

/** Слайдер для отображения списка услуг и мастеров yclients.
 * Может быть использован и в других местах. */
class YClientsSliderView : FrameLayout {

    var title: String? = null
        set(value) {
            field = value
            textViewSliderTitle?.text = value
        }
    var titleClickListener: (() -> Unit)? = null
    var onSlideListener: ((position: Int) -> Unit)? = null

    private lateinit var adapter: ListAdapter<*, out RecyclerView.ViewHolder>

    constructor(context: Context) : super(context) {
        init(context, null)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(context, attrs)
    }

    private fun init(context: Context, attrs: AttributeSet?) {
        View.inflate(context, R.layout.view_yclients_slider, this)

        attrs?.let {
            val a = context.obtainStyledAttributes(attrs, R.styleable.YClientsSliderView)
            title = a.getString(R.styleable.YClientsSliderView_title)
            val loadingRes = a.getResourceId(R.styleable.YClientsSliderView_loadingLayoutRes, -1)
            if (loadingRes != -1) {
                viewFlipperSlider.setLoadingView(loadingRes)
            }
            a.recycle()
        }

        recyclerViewSlider.attachSnapHelperWithListener(
            snapHelper = PagerSnapHelper(),
            behavior = SnapOnScrollListener.Behavior.NOTIFY_ON_SCROLL_STATE_IDLE,
            onSnapPositionChangeListener = object : OnSnapPositionChangeListener {
                override fun onSnapPositionChange(position: Int) {
                    onSlideListener?.invoke(position)
                }
            }
        )
        recyclerViewSlider.addSpaceItemDecoration()

        constraintLayoutSliderHeader.setOnClickListener {
            titleClickListener?.invoke()
        }
    }

    fun setAdapter(adapter: ListAdapter<*, out RecyclerView.ViewHolder>) {
        this.adapter = adapter
        recyclerViewSlider.adapter = adapter
    }

    fun setStateFromResult(result: LoadableResult<*>) {
        viewFlipperSlider.setStateFromResult(result)
    }

    fun scrollToPosition(pos: Int) {
        recyclerViewSlider.scrollToPosition(pos)
    }

    fun setEmptyView(@LayoutRes layout: Int) {
        frameLayoutYClientsSliderEmptyViewContainer.removeAllViews()
        val emptyView = LayoutInflater.from(context)
            .inflate(layout, frameLayoutYClientsSliderEmptyViewContainer, false)
        frameLayoutYClientsSliderEmptyViewContainer.addView(emptyView)
        recyclerViewSlider.emptyView = emptyView
        recyclerViewSlider.onEmptyViewStateChangeListener = {
            constraintLayoutSliderHeader.isClickable = it != View.VISIBLE
        }
    }
}
