package com.example.izi.presentation.dashboard

import com.example.izi.data.model.YCompanySort

data class CompaniesRequestParams(
    val cityId: String,
    val query: String? = null,
    val lat: Double? = null,
    val lng: Double? = null,
    val sort: YCompanySort? = null
)
