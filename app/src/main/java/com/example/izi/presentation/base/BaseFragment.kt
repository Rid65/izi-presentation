package com.example.izi.presentation.base

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.activity.addCallback
import androidx.annotation.CallSuper
import androidx.annotation.MainThread
import androidx.fragment.app.Fragment
import androidx.fragment.app.createViewModelLazy
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.izi.pluginframework.BasePlugin
import com.example.izi.pluginframework.LifecycleEvent
import dagger.android.support.AndroidSupportInjection
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import javax.inject.Inject
import timber.log.Timber

abstract class BaseFragment : Fragment() {

    abstract val layoutResId: Int
    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewBindingsDisposable: CompositeDisposable
    private val plugins = mutableListOf<BasePlugin>()

    @SuppressWarnings("deprecation")
    override fun onAttach(activity: Activity) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            AndroidSupportInjection.inject(this)
        }
        super.onAttach(activity)
    }

    override fun onAttach(context: Context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            AndroidSupportInjection.inject(this)
        }
        super.onAttach(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        initPlugins()
        dispatchEventToPlugins(LifecycleEvent.BeforeOnCreate(savedInstanceState))
        super.onCreate(savedInstanceState)
        dispatchEventToPlugins(LifecycleEvent.OnCreate(savedInstanceState))
        callInitOperations()
        Timber.d("onCreate $this")
    }

    override fun onResume() {
        dispatchEventToPlugins(LifecycleEvent.OnResume)
        super.onResume()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return if (view != null) view else inflater.inflate(layoutResId, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dispatchEventToPlugins(LifecycleEvent.OnViewCreated(view, savedInstanceState))
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        dispatchEventToPlugins(LifecycleEvent.OnActivityCreated(savedInstanceState))
        viewBindingsDisposable = CompositeDisposable()
        Timber.d("onActivityCreated $this")
        onSetupLayout(savedInstanceState)
        onBindViewModel()
    }

    override fun onPause() {
        dispatchEventToPlugins(LifecycleEvent.OnPause)
        super.onPause()
    }

    override fun onStop() {
        dispatchEventToPlugins(LifecycleEvent.OnStop(this))
        super.onStop()
    }

    override fun onDestroyView() {
        dispatchEventToPlugins(LifecycleEvent.OnDestroyView(this))
        super.onDestroyView()
        Timber.d("onDestroyView $this")
        viewBindingsDisposable.clear()
    }

    override fun onDestroy() {
        Timber.d("onDestroy $this")
        dispatchEventToPlugins(LifecycleEvent.OnDestroy)
        releasePlugins()
        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        dispatchEventToPlugins(LifecycleEvent.OnActivityResult(requestCode, resultCode, data))
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        dispatchEventToPlugins(LifecycleEvent.OnRequestPermissionsResult(requestCode, permissions, grantResults))
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        dispatchEventToPlugins(LifecycleEvent.OnSaveInstanceState(outState))
    }

    /**
     * Вызывать методы вью модели, которые получают данные из репозиториев
     */
    abstract fun callInitOperations()

    /**
     * В этом методе производить все настройки вью фрагмента
     * установка OnClickListener, RecyclerView.Adapter/LayoutManager, etc
     */
    abstract fun onSetupLayout(savedInstanceState: Bundle?)

    /**
     * В этом методе производить подписки на изменения значений в лайвдатах у ViewModel
     */
    abstract fun onBindViewModel()

    // region Plugins
    @CallSuper
    protected open fun initPlugins() {
        addPlugin(HideKeyboardPlugin())
    }

    protected fun addPlugin(plugin: BasePlugin) {
        plugins.add(plugin)
    }

    private fun dispatchEventToPlugins(event: LifecycleEvent) {
        plugins.forEach { it.onLifecycleEvent(event) }
    }

    private fun releasePlugins() {
        plugins.clear()
    }

    fun removePlugins(pluginClass: Class<out BasePlugin>) {
        for (i in plugins.size - 1 downTo 0) {
            if (pluginClass.isInstance(plugins[i])) {
                plugins.removeAt(i)
            }
        }
    }
    // end region Plugins

    protected infix fun <T> LiveData<T>.observe(block: (T) -> Unit) {
        observe(this@BaseFragment, Observer<T> { t -> block.invoke(t) })
    }

    protected fun <T> observeSavedStateHandleLiveData(code: String, block: (T) -> Unit) {
        findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData<T>(code)?.observe { t ->
            block.invoke(t)
            findNavController().currentBackStackEntry?.savedStateHandle?.remove<T>(code)
        }
    }

    protected fun Disposable.autoDispose() {
        viewBindingsDisposable.add(this)
    }

    @MainThread
    inline fun <reified VM : ViewModel> Fragment.iziViewModels() =
        createViewModelLazy(VM::class, { this.viewModelStore }, { viewModelFactory })

    @MainThread
    inline fun <reified VM : ViewModel> Fragment.iziActivityViewModels() =
        createViewModelLazy(VM::class, { requireActivity().viewModelStore }, {
            if (requireActivity() is BaseActivity) {
                (requireActivity() as BaseActivity).viewModelFactory
            } else {
                requireActivity().defaultViewModelProviderFactory
            }
        })

    protected fun setOnBackPressedListener(callback: () -> Unit): OnBackPressedCallback {
        return requireActivity().onBackPressedDispatcher.addCallback(this) {
            callback.invoke()
        }
    }

    val previousSavedStateHandle: SavedStateHandle? by lazy {
        findNavController().previousBackStackEntry?.savedStateHandle
    }
}
