package com.example.izi

import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class IziApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerApplicationComponent
            .factory()
            .create(this)
    }

}
