package com.example.izi.extensions

import android.content.Intent
import android.net.Uri
import android.provider.Settings
import android.util.TypedValue
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import kotlin.math.max

fun Fragment.hideSoftKeyboard() {
    activity?.hideSoftKeyboard()
}

fun Fragment.hideSoftKeyboard(view: View) {
    activity?.hideSoftKeyboard(view)
}

fun Fragment.showSoftKeyboard(showFlags: Int = InputMethodManager.SHOW_FORCED, hideFlags: Int = 0) {
    activity?.showSoftKeyboard(showFlags, hideFlags)
}

fun Fragment.showSoftKeyboard(view: View, showFlags: Int = 0) {
    activity?.showSoftKeyboard(view, showFlags)
}

fun Fragment.errorSnackbar(
    @StringRes message: Int
) {
    showErrorSnackbar(requireContext().getString(message), null) {}
}

fun Fragment.errorSnackbar(
    message: String,
    actionText: String? = null,
    action: () -> Unit
) {
    showErrorSnackbar(message, actionText, action = action)
}

fun Fragment.errorSnackbar(
    message: String,
    maxLines: Int? = null
) {
    showErrorSnackbar(message, maxLines = maxLines) {}
}

fun Fragment.errorSnackbar(message: String) {
    showErrorSnackbar(message, null) {}
}

fun Fragment.showToast(message: String) {
    view?.let {
        Snackbar.make(it, message, Snackbar.LENGTH_LONG).show()
    }
}

fun Fragment.openBrowser(address: String?) {
    Intent(Intent.ACTION_VIEW, Uri.parse(address)).apply {
        startActivity(this)
    }
}

// unresolved передавать тему
fun Fragment.sendEmailIntent(email: String) {
    val intent = Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", email, null))
    intent.putExtra(Intent.EXTRA_SUBJECT, requireActivity().getString(R.string.write_us_email_subject))
    intent.putExtra(Intent.EXTRA_TEXT, "")
    if (intent.resolveActivity(requireActivity().packageManager) != null) {
        startActivity(Intent.createChooser(intent, requireActivity().getString(R.string.send_email)))
    } else {
        Toast.makeText(requireActivity(), R.string.application_not_fount, Toast.LENGTH_LONG).show()
    }
}

private fun Fragment.showErrorSnackbar(
    message: String,
    actionText: String? = null,
    maxLines: Int? = null,
    action: () -> Unit
) {
    view?.let {
        val snackbar = Snackbar.make(it, message, Snackbar.LENGTH_INDEFINITE).apply {
            setBackgroundTint(resources.getColorCompat(R.color.error_snackbar_background))
            setTextColor(resources.getColorCompat(R.color.snackbar_text_color))
            setActionTextColor(resources.getColorCompat(R.color.snackbar_text_color))

            val wordsCount = message.split("\\s+|\\r|\\n".toRegex()).size
            val calculatedDuration = wordsCount * 300 + 1000
            duration = max(calculatedDuration, 2000)

            if (actionText != null) {
                setAction(actionText) { action() }
            }
        }

        // уменьшаем размер текста
        val shackbarView = snackbar.view
        val textViewMessage = shackbarView.findViewById(com.google.android.material.R.id.snackbar_text) as TextView
        maxLines?.let { textViewMessage.maxLines = it }
        textViewMessage.setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.error_text_caption))
        val textViewAction = shackbarView.findViewById(com.google.android.material.R.id.snackbar_action) as TextView
        textViewAction.setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.error_text_caption))

        snackbar.show()
    }
}
