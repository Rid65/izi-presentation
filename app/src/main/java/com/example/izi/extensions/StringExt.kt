package com.example.izi.extensions

import android.content.Context
import android.os.Build
import android.text.Html
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.SpannedString
import androidx.annotation.ColorRes
import androidx.core.text.buildSpannedString
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import java.util.regex.Pattern
import java.util.zip.DataFormatException

/** Valid only if exact match with russian pattern */
val String.isFullyMatchesRussianPattern: Boolean
    get() = extractPhone(this).run { startsWith("7") && length == 11 }

/**
 *  Приводит номер карты к формату "* 4567"
 *
 *  @param withoutPrefix признак того, что не нужно показывать символ '*' в начале
 */
fun String?.toShortCardNumberFormat(withoutPrefix: Boolean = false): String {
    return when {
        this == null -> ""
        this.length < 4 -> this
        else -> {
            buildString {
                if (!withoutPrefix) append("*")
                append(this@toShortCardNumberFormat.substring(this@toShortCardNumberFormat.length - 4))
            }
        }
    }
}

/**
 * Get ruble price without ruble symbol attached
 * */
val String.rublePriceValue: Double
    get() = replace("₽", "").trim().toDoubleOrNull()?.takeIf { it > 0 } ?: 0.0

/** Remove all spaces from String */
val String.withoutSpaces: String?
    get() = replace(" ", "")

val String?.withRuble: String?
    get() = this.plus(" ₽")

fun String.fromBirthday(): DateOnly? {
    val format = SimpleDateFormat(DateFormatEnum.BirthdayFormat.format, Locale.getDefault())
    val date = try {
        format.parse(this) ?: Date()
    } catch (e: Exception) {
        Date()
    }
    return DateOnly(date)
}

/** Semi-valid if matches pattern, no need to fully match phone pattern */
val String.isValidPhoneNumber: Boolean
    get() = android.util.Patterns.PHONE.matcher(this).matches()

fun String.toDate(): DateTime {
    val apiDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault())
    val date = try {
        apiDateFormat.parse(this) ?: Date()
    } catch (e: DataFormatException) {
        Date()
    }
    return DateTime(date)
}

/**
 * Date from short hour and minutes
 * f.e. 02:55
 */
fun String.fromHourAndMinutes(): DateTime {
    val apiDateFormat = SimpleDateFormat("HH:mm", Locale.getDefault())
    val date = try {
        apiDateFormat.parse(this) ?: Date()
    } catch (e: DataFormatException) {
        Date()
    }
    return DateTime(date)
}

/**
 * Date from short day and month
 * f.e. 01.12
 */
fun String.fromShortDayAndMonth(): Date {
    val apiDateFormat = SimpleDateFormat("dd.MM", Locale.getDefault())
    val date = try {
        apiDateFormat.parse(this) ?: Date()
    } catch (e: DataFormatException) {
        Date()
    }
    return DateOnly(date)
}

fun String?.fromHtml(): Spanned {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Html.fromHtml(this ?: "", Html.FROM_HTML_MODE_LEGACY)
    } else {
        Html.fromHtml(this ?: "")
    }
}

fun String.makeClickableSpan(
    context: Context,
    @ColorRes colorId: Int = R.color.clickable_span_color,
    callback: () -> Unit
): SpannableString {
    val clickableSpan = CustomClickableSpan(context, callback, colorId)

    return SpannableString(this).apply {
        setSpan(
            clickableSpan,
            0,
            this.length,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
    }
}

/**
 * Для КупиКупона
 * Разделяет номера и делает их кликабельными
 */
fun String.makeSpannablePhone(context: Context, clickCallback: (String) -> Unit): SpannedString? {
    return buildSpannedString {
        this@makeSpannablePhone.split("\n")
            .forEach { splitPhoneString ->
                append(splitPhoneString.makeClickableSpan(context) {
                    clickCallback.invoke(splitPhoneString)
                })
                appendln()
            }
    }
}

/**
 *  Метод объединяет строки аналогично joinToString, но с сохранением Spannable параметров
 */
fun <T> Iterable<T>.joinToSpannedString(
    separator: CharSequence = ", ",
    prefix: CharSequence = "",
    postfix: CharSequence = "",
    limit: Int = -1,
    truncated: CharSequence = "...",
    transform: ((T) -> CharSequence)? = null
): SpannedString {
    return SpannedString(joinTo(SpannableStringBuilder(), separator, prefix, postfix, limit, truncated, transform))
}

fun String.parseQueryToMap(): Map<String, String> {
    val map = mutableMapOf<String, String>()
    val pairs = this.split("&")
    pairs.forEach { pair ->
        val keyValuePair = pair.split("=")
        val key = keyValuePair[0]
        val value = keyValuePair[1]
        map[key] = value
    }
    return map
}

fun String?.lowerTitleTag(): String? {
    var text = this
    val pattern = Pattern.compile("(\\u003c|<)h\\d(>|\\u003e)[А-ЯA-Z\\n ]*(\\u003c|<)/h\\d(>|\\u003e)")
    val matcher = pattern.matcher(this)
    while (matcher.find()) {
        var group = matcher.group()
        group = group.replace("\n", "")
            .replace(Regex("<h\\d>"), "")
            .replace(Regex("</h\\d>"), "")
        var lowerGroup = ""
        if (group.length > 1) {
            lowerGroup = group.substring(0, 1) + group.substring(1).toLowerCase(Locale.getDefault())
        }
        text = text?.replace(group, lowerGroup)
    }
    return text
}