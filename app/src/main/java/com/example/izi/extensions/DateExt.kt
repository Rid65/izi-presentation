package com.example.izi.extensions

import android.content.res.Resources
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale

/**
 * Строка в формате "d MMMM yyyy" ("21 декабря 2019")
 */
fun Date?.toFullDateFormat(): String {
    if (this == null) return ""

    val dateFormat = SimpleDateFormat("d MMMM yyyy", Locale.getDefault())
    return dateFormat.format(this)
}

/**
 * Строка в формате "dd.MM.yyyy" ("21.05.2019")
 */
fun Date?.dateAllNumber(): String {
    if (this == null) return ""

    val dateFormat = SimpleDateFormat(DateFormatEnum.BirthdayFormat.format, Locale.getDefault())
    return dateFormat.format(this)
}

/**
 * День месяца ("21")
 */
fun Date?.dayOfMonth(): String {
    if (this == null) return ""

    val dateFormat = SimpleDateFormat("dd", Locale.getDefault())
    return dateFormat.format(this)
}

/**
 * День месяца без начальных нулей ("1", "21")
 */
fun Date?.dayOfMonthShort(): String {
    if (this == null) return ""

    val dateFormat = SimpleDateFormat("d", Locale.getDefault())
    return dateFormat.format(this)
}

/**
 * День недели ("ПН")
 */
fun Date?.dayOfWeekShort(): String {
    if (this == null) return ""

    val dateFormat = SimpleDateFormat("EEE", Locale.getDefault())
    return dateFormat.format(this)
}

/**
 * Day and month in short format
 * f.e. 01.12
 */
fun Date?.shortDayAndMonth(): String {
    if (this == null) return ""

    val dateFormat = SimpleDateFormat("dd.MM", Locale.getDefault())
    return dateFormat.format(this)
}

/**
 * Месяц "Январь"
 */
fun Date?.month(): String {
    if (this == null) return ""

    val dateFormat = SimpleDateFormat("LLLL", Locale.getDefault())
    return dateFormat.format(this)
}

/**
 * День месяца и время ("8 апреля 12:00")
 */
fun Date?.dateAndTime(withLetter: Boolean = false): String {
    if (this == null) return ""
    val format = if (!withLetter) "d MMMM HH:mm" else "d MMMM в HH:mm"
    val dateFormat = SimpleDateFormat(format, Locale.getDefault())
    return dateFormat.format(this)
}

/**
 * Строка в формате "HH:mm" ("12:00")
 */
fun Date?.toStandardTimeFormat(): String {
    return if (this == null) ""
    else {
        val timeFormat = SimpleDateFormat("HH:mm", Locale.getDefault())
        timeFormat.format(this)
    }
}

/**
 * Строка в формате "H:mm" ("9:00")
 */
fun Date?.toShortTimeFormat(): String {
    return if (this == null) ""
    else {
        val timeFormat = SimpleDateFormat("H:mm", Locale.getDefault())
        timeFormat.format(this)
    }
}

/**
 * День и месяц ("21 марта")
 */
fun Date?.dayOfMonthFull(): String {
    if (this == null) return ""
    val dateFormat = SimpleDateFormat("d MMMM", Locale.getDefault())
    return dateFormat.format(this)
}

/**
 * День и месяц в сокращенном формате ("21 мар") без точки
 * В английском точки в конце по умолчанию нет, в русском есть
 */
fun Date?.dayAndMonthShort(): String {
    if (this == null) return ""
    val date = SimpleDateFormat("d MMM", Locale.getDefault()).format(this)
    return if (date.last() == '.') date.dropLast(1) else date
}

/**
 * День, месяц и день недели ("Понедельник, 21 марта")
 */
fun Date?.dayOfMonthWithDayOfWeek(): String {
    if (this == null) return ""
    val dateFormat = SimpleDateFormat("EEEE, d MMMM", Locale.getDefault())
    return dateFormat.format(this)
}

/**
 * Общий метод для вывода дат (без времени)
 * Использует префиксы типа "Вчера", "Сегодня" (как вариант можно сделать методы в DateTime классе типа isToday(), isYesterday)
 * Применяет разные форматы в зависимости от года
 *
 * Сегодня
 * Вчера
 * 12 мая
 * 12 мая 2020
 *
 * */
fun Date?.prettyDate(resources: Resources): String {
    this ?: return ""
    return when {
        isToday() -> resources.getString(R.string.date_today)
        isYesterday() -> resources.getString(R.string.date_yesterday)
        else -> {
            if (isCurrentYear()) {
                dayOfMonthFull()
            } else {
                this.toFullDateFormat()
            }
        }
    }
}

/**
 *  Год ("2020")
 */
fun Date?.year(): String {
    if (this == null) return ""
    val dateFormat = SimpleDateFormat("yyyy", Locale.getDefault())
    return dateFormat.format(this)
}

/**
 * Дата и время в формате ("10 января 2019 в 12:00")
 */
fun DateTime?.dateAndTimeWithYear(): String {
    if (this == null) return ""
    val dateFormat =
        SimpleDateFormat("d MMMM yyyy 'в' HH:mm", Locale.getDefault())
    return dateFormat.format(this)
}

/**
 * Сравнить дату с [comparedDate]
 * Возвращает true - если один и тот же день.
 * */

fun Date?.isTheSameDay(comparedDate: Date?): Boolean {
    if (this == null || comparedDate == null) return false
    val calendar1 = Calendar.getInstance().apply { time = this@isTheSameDay }
    val calendar2 = Calendar.getInstance().apply { time = comparedDate }
    return calendar1.get(Calendar.DAY_OF_YEAR) == calendar2.get(Calendar.DAY_OF_YEAR) &&
            calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR)
}

/**
 * Сравнить дату с текущей
 * Возвращает true - если этот день сегодня
 * */

fun Date?.isToday(): Boolean {
    if (this == null) return false
    return isTheSameDay(
        comparedDate = Calendar.getInstance()
            .time
    )
}

/**
 * Сравнить дату с вчерашней
 * Возвращает true - если этот день был вчера
 * */

fun Date?.isYesterday(): Boolean {
    if (this == null) return false
    return isTheSameDay(
        comparedDate = Calendar.getInstance()
            .apply { add(Calendar.DAY_OF_YEAR, -1) }
            .time
    )
}

/**
 * Сравнить год с [comparedDate]
 * Возвращает true - если один и тот же год
 * */

fun Date?.isTheSameYear(comparedDate: Date): Boolean {
    if (this == null) return false
    val calendar1 = Calendar.getInstance().apply { time = this@isTheSameYear }
    val calendar2 = Calendar.getInstance().apply { time = comparedDate }
    return calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR)
}

/**
 * Сравнить год с текущим
 * Возвращает true - если этот год текущий
 * */

fun Date?.isCurrentYear(): Boolean {
    if (this == null) return false
    return isTheSameYear(
        comparedDate = Calendar.getInstance()
            .time
    )
}

/** Simplified getter from Calendar */
fun Date.getFromCalendar(field: Int): Int {
    val calendar = Calendar.getInstance()
    calendar.time = this
    return calendar.get(field)
}

/** Check is date time is before or same time of toCompare date time */
fun Date.beforeOrEqual(toCompare: Date): Boolean {
    return compareTo(toCompare) <= 0
}

/** Check is date time is after or same time of toCompare date time */
fun Date.afterOrEqual(toCompare: Date): Boolean {
    return compareTo(toCompare) >= 0
}

/**
 * Метод обнуляет время у даты
 */
fun Date.clearTime(): Date {
    val cal: Calendar = Calendar.getInstance().apply { time = this@clearTime }
    cal.clearToStartOfADay()
    return cal.time
}

/**
 * Вычитает одну секунду (требуется для получения списка транзакций и суммы за период)
 */
fun Date.subtractOneSecond(): Date {
    val cal: Calendar = Calendar.getInstance().apply { time = this@subtractOneSecond }
    cal.add(Calendar.SECOND, -1)
    return cal.time
}

/**
 * Прибавляет (требуется для получения списка транзакций и суммы за период)
 */
fun Date.addOneDay(): Date {
    val cal: Calendar = Calendar.getInstance().apply { time = this@addOneDay }
    cal.add(Calendar.DAY_OF_MONTH, 1)
    return cal.time
}

fun Date.plusDays(count: Int): Date {
    return Calendar.getInstance().apply {
        time = this@plusDays
        add(Calendar.DATE, count)
    }.time
}

fun Date.plusMinutes(count: Int): Date {
    return Calendar.getInstance().apply {
        time = this@plusMinutes
        add(Calendar.MINUTE, count)
    }.time
}

fun Date.clearSecondsAndMilliseconds(): Date {
    return Calendar.getInstance().apply {
        timeInMillis = this@clearSecondsAndMilliseconds.time
        set(Calendar.SECOND, 0)
        set(Calendar.MILLISECOND, 0)
    }.time
}