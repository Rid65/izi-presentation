package com.example.izi.extensions

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.content.ContextCompat

fun Activity.hideSoftKeyboard() {
    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
    imm?.hideSoftInputFromWindow(currentFocus?.windowToken, 0)
}

fun Activity.hideSoftKeyboard(view: View) {
    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
    imm?.hideSoftInputFromWindow(view.windowToken, 0)
}

fun Activity.callPhone(phone: String) {
    val intent = Intent(Intent.ACTION_DIAL)
    intent.data = Uri.parse("tel:$phone")
    ContextCompat.startActivity(this, intent, null)
}

fun Activity.sendEmail(email: String) {
    val intent = Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", email, null))
    intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.write_us_email_subject))
    intent.putExtra(Intent.EXTRA_TEXT, "")
    if (intent.resolveActivity(packageManager) != null) {
        startActivity(Intent.createChooser(intent, getString(R.string.send_email)))
    } else {
        Toast.makeText(this, R.string.application_not_fount, Toast.LENGTH_LONG).show()
    }
}

fun Activity.showSoftKeyboard(showFlags: Int = InputMethodManager.SHOW_FORCED, hideFlags: Int = 0) {
    val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.toggleSoftInput(showFlags, hideFlags)
}

fun Activity.showSoftKeyboard(view: View, showFlags: Int = 0) {
    val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.showSoftInput(view, showFlags)
}
