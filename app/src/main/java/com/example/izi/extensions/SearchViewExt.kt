package com.example.izi.extensions

import android.widget.EditText
import android.widget.ImageView
import androidx.annotation.ColorRes
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import com.example.izi.R

fun SearchView.setTextColor(@ColorRes id: Int) {
    searchEditText.apply {
        setTextColor(ContextCompat.getColor(context, id))
    }
}

fun SearchView.setHintTextColor(@ColorRes id: Int) {
    searchEditText.apply {
        setHintTextColor(ContextCompat.getColor(context, id))
    }
}

val SearchView.searchEditText: EditText
    get() = findViewById(androidx.appcompat.R.id.search_src_text)

val SearchView.clearButton: ImageView
    get() = findViewById(R.id.search_close_btn)
