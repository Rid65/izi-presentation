package com.example.izi.data

import com.example.izi.data.remote.*
import com.google.gson.GsonBuilder
import com.google.gson.JsonSyntaxException
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import retrofit2.HttpException
import timber.log.Timber
import java.io.IOException
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

typealias Loading<T> = LoadableResult.Loading<T>
typealias Success<T> = LoadableResult.Success<T>
typealias Failure<T> = LoadableResult.Failure<T>

/**
 * Usage:
 * when (result) {
 *     is Loading -> { ... }
 *     is Success -> { value ->... }
 *     is Failure -> { throwable ->... }
 * }
 */
sealed class LoadableResult<R> {
    class Loading<R> : LoadableResult<R>()
    class Success<R>(val value: R) : LoadableResult<R>()
    class Failure<R>(val throwable: Throwable) : LoadableResult<R>() {
        val error = throwable.parseError()
    }

    companion object {
        fun <R> loading(): LoadableResult<R> =
            Loading()

        fun <R> success(value: R): LoadableResult<R> =
            Success(value)

        fun <R> failure(throwable: Throwable): LoadableResult<R> =
            Failure(throwable)
    }

    val isLoading: Boolean get() = this is Loading

    val isSuccess: Boolean get() = this is Success

    val isFailure: Boolean get() = this is Failure

    /**
     * Usage:
     * val product: Product? = result.getOrNull()
     */
    fun getOrNull(): R? = when (this) {
        is Success -> value
        else -> null
    }

    /**
     * Usage:
     * val product: Product = result.getOrDefault {
     *     calculateDefaultValue()
     * }
     */
    fun getOrDefault(default: () -> R): R = when (this) {
        is Success -> value
        else -> default()
    }

    /**
     * Usage:
     * val product: Product = result.getOrDefault(Product.stub)
     */
    fun getOrDefault(defaultValue: R): R = when (this) {
        is Success -> value
        else -> defaultValue
    }

    /**
     * Converts a Success to a Failure if the predicate is not satisfied.
     * Usage:
     * val loadable = Success(Product()).filter {
     *     it.isAvailable()
     * }
     */
    inline fun filter(predicate: (R) -> Boolean): LoadableResult<R> = when (this) {
        is Loading -> loading()
        is Failure -> failure(
            throwable
        )
        is Success -> if (predicate(value)) {
            success(value)
        } else {
            failure(FilterException())
        }
    }

    /**
     * The given function is applied if this is a Success.
     * Usage:
     * Success(Product()).map {
     *     Pair(position, it)
     * }
     */
    inline fun <C> map(f: (R) -> C): LoadableResult<C> = when (this) {
        is Loading -> loading()
        is Failure -> failure(
            throwable
        )
        is Success -> success(
            f(value)
        )
    }

    /**
     * Applies ifLoading if this is a Loading
     * or ifFailure if this is a Failure
     * or ifSuccess if this is a Success.
     */
    inline fun <C> fold(
        ifLoading: () -> C,
        ifFailure: (t: Throwable) -> C,
        ifSuccess: (R) -> C
    ): C = when (this) {
        is Loading -> ifLoading()
        is Failure -> ifFailure(throwable)
        is Success -> ifSuccess(value)
    }

    /**
     * Usage:
     * result.doOnComplete {
     *     val value = it.getOrNull()
     *     handleValue(value)
     * }
     */
    inline fun doOnComplete(operation: (LoadableResult<R>) -> Unit) {
        when (this) {
            is Loading -> { /* Skip it */
            }
            is Failure -> operation(
                failure(
                    throwable
                )
            )
            is Success -> operation(
                success(
                    value
                )
            )
        }
    }

    /**
     * Usage:
     * result.doOnLoading {
     *     showLoading()
     * }
     */
    inline fun doOnLoading(operation: () -> Unit) {
        when (this) {
            is Loading -> operation()
            is Failure -> { /* Skip it */
            }
            is Success -> { /* Skip it */
            }
        }
    }

    /**
     * Usage:
     * result.doOnSuccess {
     *     val value = it.getOrNull()
     *     handleValue(value)
     * }
     */
    inline fun doOnSuccess(operation: (R) -> Unit) {
        when (this) {
            is Loading -> { /* Skip it */
            }
            is Failure -> { /* Skip it */
            }
            is Success -> operation(value)
        }
    }

    /**
     * Usage:
     * result.doOnSuccess {
     *     val value = it.getOrNull()
     *     handleValue(value)
     * }
     */
    inline fun doOnFailure(operation: (ParsedError) -> Unit) {
        when (this) {
            is Loading -> { /* Skip it */
            }
            is Failure -> operation(error)
            is Success -> { /* Skip it */
            }
        }
    }
}

fun <T> Single<T>.convert(block: (LoadableResult<T>) -> Unit): Disposable {
    return doOnSubscribe {
        block.invoke(LoadableResult.loading())
    }.subscribe({ value ->
        block.invoke(LoadableResult.success(value))
    }, { throwable ->
        block.invoke(LoadableResult.failure(throwable))
    })
}

fun <T> Flowable<T>.convert(block: (LoadableResult<T>) -> Unit): Disposable {
    return doOnSubscribe {
        block.invoke(Loading())
    }.subscribe(
        { value ->
            block.invoke(Success(value))
        }, { throwable ->
            block.invoke(Failure(throwable))
        })
}

fun <T> Observable<T>.convert(block: (LoadableResult<T>) -> Unit): Disposable {
    return doOnSubscribe {
        block.invoke(LoadableResult.loading())
    }.subscribe({ value ->
        block.invoke(LoadableResult.success(value))
    }, { throwable ->
        block.invoke(LoadableResult.failure(throwable))
    })
}

fun <T> Maybe<T>.convert(defaultValue: T, block: (LoadableResult<T>) -> Unit): Disposable {
    return doOnSubscribe {
        block.invoke(LoadableResult.loading())
    }.subscribe({ value ->
        block.invoke(LoadableResult.success(value))
    }, { throwable ->
        block.invoke(LoadableResult.failure(throwable))
    }, {
        block.invoke(LoadableResult.success(defaultValue))
    })
}

class FilterException : Throwable("Filter do not match a given condition through predicate")

/** Manually designed exception if the refresh token was not decrypted and the server answered 401 errors */
class TokenRetrievalException : Throwable("Token decoding error ")

private fun Throwable.parseError(): ParsedError {
    Timber.e(this)

    val code = DEFAULT_ERROR_CODE
    val message = DEFAULT_ERROR_MESSAGE

    return when {
        isNetworkError || (cause?.isNetworkError ?: false) -> NetworkError(code, message)
        /** Unavailable service error */
        isUnavailableServiceError -> UnavailableServiceError(code, message)
        /** General Http error */
        this is HttpException -> {
            val body = response()?.errorBody()
            val gson = GsonBuilder().create()
            var error: GeneralError? = null
            try {
                val apiError = gson.fromJson(body?.string(), ApiErrorResponse::class.java)
                error = apiError?.toGeneralError()
            } catch (ioEx: IOException) {
                Timber.e(ioEx)
            } catch (isEx: IllegalStateException) {
                Timber.e(isEx)
            } catch (isEx: JsonSyntaxException) {
                Timber.e(isEx)
            }
            error ?: GeneralError(code, message)
        }
        /** Map API exception into indoor error in case if local refresh token is incorrect for some reason */
        this is TokenRetrievalException -> TokenRetrievalError(code, message)
        /** Fallback from biometry */
        this is javax.crypto.IllegalBlockSizeException -> FallBackFromBiometryError(code, message)
        else -> {
            GeneralError(code, message)
        }
    }
}

private val Throwable.isNetworkError: Boolean
    get() = when (this) {
        is ConnectException,
        is UnknownHostException,
        is SocketTimeoutException -> true
        else -> false
    }

private val Throwable.isUnavailableServiceError: Boolean
    get() = this is HttpException && code() in listOf(500, 503)

val Throwable.isTokenError
    get() = this is HttpException && code() == 401
