package com.example.izi.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName

/** Компания YClients */
@Parcelize
data class YCompany(
    @SerializedName("id") val id: String,
    /** Название компании.
     * Example: Студия красоты MixTon */
    @SerializedName("title") val title: String?,
    /** Описание компании.
     * Example: Салон красоты */
    @SerializedName("shortDescription") val shortDescription: String?,
    @SerializedName("phone") val phone: String?,
    @SerializedName("address") val address: String?,
    @SerializedName("geolocation") val geolocation: Geolocation?,
    @SerializedName("image") val image: String?
) : Similarable<YCompany>, Parcelable, Dashboardable {
    override fun areItemsTheSame(other: YCompany): Boolean {
        return id == other.id
    }

    override fun areContentsTheSame(other: YCompany): Boolean {
        return this == other
    }
}

/** Сортировка компаний YClients. Передается в запросе на сервер. */
enum class YCompanySort {

    /** По названию компании по возврастанию */
    @SerializedName("title_asc")
    TITLE_ASC,

    /** По названию компании по убыванию */
    @SerializedName("title_desc")
    TITLE_DESC,

    /** Сортировка по удаленности от пользователя. Передается только в паре с lat, lng. */
    @SerializedName("distance")
    DISTANCE
}