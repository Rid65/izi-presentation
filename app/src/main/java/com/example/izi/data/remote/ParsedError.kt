package com.example.izi.data.remote

import com.google.gson.annotations.SerializedName

sealed class ParsedError(val code: String, val message: String)

class NetworkError(code: String, message: String) : ParsedError(code, message)
class UnavailableServiceError(code: String, message: String) : ParsedError(code, message)
class GeneralError(code: String, message: String) : ParsedError(code, message)

/** Server forces client to retrieve token */
class TokenRetrievalError(code: String, message: String) : ParsedError(code, message)
/** Error get data from biometry */
class FallBackFromBiometryError(code: String, message: String) : ParsedError(code, message)

data class ApiErrorResponse(
    @SerializedName("error") val error: ApiError?,
    // unresolved временное поле из за ошибки бекенда
    @SerializedName("detail") val backendHack: String?
) {
    fun toGeneralError(): GeneralError {
        return GeneralError(
            error?.code ?: DEFAULT_ERROR_CODE,
            error?.message ?: backendHack ?: DEFAULT_ERROR_MESSAGE
        )
    }
}

data class ApiError(
    @SerializedName("code") val code: String?,
    @SerializedName("message") val message: String?,
    /** время блокировки */
    @SerializedName("expire") val expire: String?,
    @SerializedName("fields") val fields: List<Map<String, String>>?
)

const val DEFAULT_ERROR_CODE = "API_ERROR"
/** случай, когда смс код протух и нужно получить новый **/
const val OPERATION_TOKEN_EXPIRY = "OPERATION_TOKEN_EXPIRY"

lateinit var DEFAULT_ERROR_MESSAGE: String
