package com.example.izi.data.remote

import com.example.izi.data.model.YCompany
import com.example.izi.data.model.YCompanySort
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface YClientsApiService : BaseApiService {

    @GET("apps/yclients/cities/{cityId}/companies")
    fun getCompanies(
        @Path("cityId") cityId: String,
        @Query("limit") limit: Int = DEFAULT_LIMIT,
        @Query("offset") offset: Int = 0,
        @Query("query") query: String? = null,
        @Query("lat") lat: Double? = null,
        @Query("lng") lng: Double? = null,
        @Query("sort") sort: YCompanySort? = null
    ): Single<ListResponse<YCompany>>

}
